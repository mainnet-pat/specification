# Signatures

Bitcoin transaction generally require at least one signature in order to be valid.
Signatures prove that the intended recipient(s) of the funds being spent were involved in the creation of the transaction.
Each signature is generated using a [private key](/blockchain/cryptography/keys) and indicates that the owner of that private key approves of certain data in the transaction.
This page describes how these signature are generated, given data to be signed and a private key.
For details on what data is signed when signing a transaction and how the signature is formatted, see [Transaction Signing](/blockchain/transaction/transaction-signing).

Nexa uses [Schnorr](#schnorr-signatures) for transaction signatures.

## Schnorr Signatures

Schnorr signatures have been accepted in Bitcoin Cash since [HF-20190515](/forks/hf-20190515).
Details on the implementation of Schnorr used in Bitcoin Cash can be found [here](/forks/2019-05-15-schnorr).
Of particular importance are the following points:

- Pre-existing Public and Private Keys are valid for generating Schnorr signatures.
- Following activation of Schnorr signatures, all 64-byte signatures passed to OP_CHECKDATASIG/VERIFY and all 65-byte signatures passed to OP_CHECKSIG/VERIFY are interpretted as Schnorr signatures.
  65-byte signatures passed to OP_CHECKMULTISIG/VERIFY will trigger script failure.
- Bitcoin Cash uses the (r, s) variant of Schnorr, instead of the (e, s) variant, with an altered approach from that originally proposed by Pieter Wuille.
- Both random and deterministic k values are considered secure, though care must be taken with deterministic k values to avoid conflicts with deterministic ECDSA k values.
